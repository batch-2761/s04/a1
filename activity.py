# Create an abstract class called Animal that has the following abstract methods:
# eat(food)
# make_sound()

from abc import ABC

class Animal(ABC):
	def eat(self, food):
		pass

	def make_sound():
		pass

# Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
# Properties:
# Name
# Breed
# Age
# Methods:
# Getters
# Setters
# Implementation of abstract methods
# call()
 
class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()

		self._name = name
		self._breed = breed
		self._age = age

	def get_name(self):
		print(f'Name of dog: {self._name}')

	def set_name(self, name):
		self._name = name

	def get_breed(self):
		print(f'Breed of dog: {self._breed}')

	def set_name(self, breed):
		self._breed = breed

	def get_age(self):
		print(f'Age of dog: {self._age}')

	def set_age(self, age):
		self._age = age

	def eat(self, food):
		self._food = food
		print(f'Eaten {self._food}')

	def make_sound(self):
		print(f'Bark! Woof! Arf!')

	def call(self):
		print(f'Here {self._name}!')


class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()

		self._name = name
		self._breed = breed
		self._age = age

	def get_name(self):
		print(f'Name of dog: {self._name}')

	def set_name(self, name):
		self._name = name

	def get_breed(self):
		print(f'Breed of dog: {self._breed}')

	def set_name(self, breed):
		self._breed = breed

	def get_age(self):
		print(f'Age of dog: {self._age}')

	def set_age(self, age):
		self._age = age

	def eat(self, food):
		self._food = food
		print(f'Serve me {self._food}')

	def make_sound(self):
		print(f'Miaow! Nyaw! Nyaaaaa!')

	def call(self):
		print(f'{self._name}, come on!')
		

dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()